import React from 'react';
import './App.css';
import { Container, Header } from "semantic-ui-react";

const App = ({ children }) => (
  <Container style={{ margin: 30 }}>
    <Header as="h3" align="center">
      Pitafoo
    </Header>
    {children}
  </Container>
);
export default App;
