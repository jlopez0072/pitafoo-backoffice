import React from "react";
import {
  Tab,
  Table,
  Icon,
  Menu,
  Segment,
  Sidebar,
  Checkbox,
  Dropdown,
  Label,
  Button,
  Search,
  Grid
} from "semantic-ui-react";

const TableMenu = () => (
  <Table celled size="small">
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Nombre</Table.HeaderCell>
        <Table.HeaderCell>Telefono</Table.HeaderCell>
        <Table.HeaderCell>Estado</Table.HeaderCell>
        <Table.HeaderCell>Accion</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Menu</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Menu</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Menu</Button>
      </Table.Row>
    </Table.Body>

    <Table.Footer>
      <Table.Row />
    </Table.Footer>
  </Table>
);

const TablePlatos = () => (
  <Table celled size="small">
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Nombre</Table.HeaderCell>
        <Table.HeaderCell>Fotos</Table.HeaderCell>
        <Table.HeaderCell>Precios</Table.HeaderCell>
        <Table.HeaderCell>Menu</Table.HeaderCell>
        <Table.HeaderCell>Estado</Table.HeaderCell>
        <Table.HeaderCell>Accion</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Plato</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Plato</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Plato</Button>
      </Table.Row>
    </Table.Body>

    <Table.Footer>
      <Table.Row />
    </Table.Footer>
  </Table>
);

const TableOfertas = () => (
  <Table celled size="small">
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Nombre</Table.HeaderCell>
        <Table.HeaderCell>Tipo</Table.HeaderCell>
        <Table.HeaderCell>Precio</Table.HeaderCell>
        <Table.HeaderCell>Vence</Table.HeaderCell>
        <Table.HeaderCell>Reclamados</Table.HeaderCell>
        <Table.HeaderCell>Estado</Table.HeaderCell>
        <Table.HeaderCell>Accion</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Oferta</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Oferta</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Oferta</Button>
      </Table.Row>
    </Table.Body>

    <Table.Footer>
      <Table.Row />
    </Table.Footer>
  </Table>
);

const TableSucursales = () => (
  <Table celled size="small">
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Sector</Table.HeaderCell>
        <Table.HeaderCell>Telefono</Table.HeaderCell>
        <Table.HeaderCell>Email</Table.HeaderCell>
        <Table.HeaderCell>Vence</Table.HeaderCell>
        <Table.HeaderCell>Estado</Table.HeaderCell>
        <Table.HeaderCell>Accion</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Editar</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Editar</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Editar</Button>
      </Table.Row>
    </Table.Body>

    <Table.Footer>
      <Table.Row />
    </Table.Footer>
  </Table>
);

const panes = [
  {
    menuItem: "Menus",
    render: () => (
      <Tab.Pane>
        <Menu size="small">
          <Menu.Item>Menues</Menu.Item>
          <Menu.Item>
            <Search size="small" />
          </Menu.Item>
          <Menu.Item>
            <Button floated="right" href="http://www.google.com">
              Agregar Menu
            </Button>
          </Menu.Item>
        </Menu>
        <TableMenu />
      </Tab.Pane>
    )
  },
  {
    menuItem: "Platos",
    render: () => (
      <Tab.Pane>
        <Menu size="small">
          <Menu.Item>Platos</Menu.Item>
          <Menu.Item>
            <Search size="small" />
          </Menu.Item>
          <Menu.Item>
            <Button floated="right" href="http://www.google.com">
              Agregar
            </Button>
          </Menu.Item>
        </Menu>
        <TablePlatos />
      </Tab.Pane>
    )
  },
  {
    menuItem: "Ofertas",
    render: () => (
      <Tab.Pane>
        <Menu size="small">
          <Menu.Item>Ofertas</Menu.Item>
          <Menu.Item>
            <Search size="small" />
          </Menu.Item>
          <Menu.Item>
            <Button floated="right" href="http://www.google.com">
              Agregar
            </Button>
          </Menu.Item>
        </Menu>
        <TableOfertas />
      </Tab.Pane>
    )
  },
  {
    menuItem: "Sucursales",
    render: () => (
      <Tab.Pane>
        <Menu size="small">
          <Menu.Item>Sucursales</Menu.Item>
          <Menu.Item>
            <Search size="small" />
          </Menu.Item>
          <Menu.Item>
            <Button floated="right" href="http://www.google.com">
              Agregar
            </Button>
          </Menu.Item>
        </Menu>
        <TableSucursales />
      </Tab.Pane>
    )
  },
  { menuItem: "Suscripcion", render: () => <Tab.Pane>Tab 3 Content</Tab.Pane> },
  {
    menuItem: "Configuracion",
    render: () => <Tab.Pane>Tab 3 Content</Tab.Pane>
  }
];

const TabBussiness = () => <Tab panes={panes} />;

const SidebarVisible = () => (
  <Sidebar.Pushable as={Segment}>
    <Sidebar
      as={Menu}
      animation="Slide Along"
      icon="labeled"
      inverted
      vertical
      visible
      width="thin"
    >
      <Menu.Item as="a" href="./sidebar">
        <Icon name="Bussiness" />
        Bussiness
      </Menu.Item>
      <Menu.Item as="a" href="http://www.google.com">
        <Icon name="Shoppers" />
        Shoppers
      </Menu.Item>
    </Sidebar>

    <Sidebar.Pusher>
      <Segment basic>
        <Grid.Column>
          <TabBussiness />
        </Grid.Column>
      </Segment>
    </Sidebar.Pusher>
  </Sidebar.Pushable>
);

export default SidebarVisible;
