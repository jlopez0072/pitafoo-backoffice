import React from "react";
import {
  Header,
  Icon,
  Menu,
  Segment,
  Sidebar,
  Dropdown,
  Label,
  Button,
  Search,
  Grid
} from "semantic-ui-react";

import TableData from "./table";

const SidebarVisible = () => (
  <Sidebar.Pushable as={Segment}>
    <Sidebar
      as={Menu}
      animation="Slide Along"
      icon="labeled"
      inverted
      vertical
      visible
      width="thin"
    >
      <Menu.Item as="a" href="./sidebar">
        <Icon name="Bussiness" />
        Bussiness
      </Menu.Item>
      <Menu.Item as="a" href="http://www.google.com">
        <Icon name="Shoppers" />
        Shoppers
      </Menu.Item>
    </Sidebar>

    <Sidebar.Pusher>
      <Segment basic>
        <Grid.Column>
          <Menu size="small">
            <Menu.Item>Agente</Menu.Item>
            <Menu.Item>
              <Dropdown text="Tipo">
                <Dropdown.Menu>
                  <Dropdown.Item text="New" />
                  <Dropdown.Item text="Open..." description="ctrl + o" />
                  <Dropdown.Item text="Save as..." description="ctrl + s" />
                  <Dropdown.Item text="Rename" description="ctrl + r" />
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Item>
            <Menu.Item>
              <Dropdown text="Provincia">
                <Dropdown.Menu>
                  <Dropdown.Item text="New" />
                  <Dropdown.Item text="Open..." description="ctrl + o" />
                  <Dropdown.Item text="Save as..." description="ctrl + s" />
                  <Dropdown.Item text="Rename" description="ctrl + r" />
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Item>
            <Menu.Item>
              <Dropdown text="Estado">
                <Dropdown.Menu>
                  <Dropdown.Item text="New" />
                  <Dropdown.Item text="Open..." description="ctrl + o" />
                  <Dropdown.Item text="Save as..." description="ctrl + s" />
                  <Dropdown.Item text="Rename" description="ctrl + r" />
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Item>
            <Menu.Item>
              <Button href="http://www.google.com">Importar</Button>
            </Menu.Item>
            <Menu.Item>
              <Search size="small" />
            </Menu.Item>
            <Menu.Item>
              <Button floated="right" href="http://www.google.com">
                Agregar Negocio
              </Button>
            </Menu.Item>
          </Menu>
        </Grid.Column>
        <TableData />
      </Segment>
    </Sidebar.Pusher>
  </Sidebar.Pushable>
);

export default SidebarVisible;
