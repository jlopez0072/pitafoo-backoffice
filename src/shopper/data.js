import React from "react";
import {
  Icon,
  Menu,
  Segment,
  Sidebar,
  Dropdown,
  Search,
  Grid
} from "semantic-ui-react";

import TableData from "./info";

const SidebarVisible = () => (
  <Sidebar.Pushable as={Segment}>
    <Sidebar
      as={Menu}
      animation="Slide Along"
      icon="labeled"
      inverted
      vertical
      visible
      width="thin"
    >
      <Menu.Item as="a" href="./sidebar">
        <Icon name="Bussiness" />
        Bussiness
      </Menu.Item>
      <Menu.Item as="a" href="http://www.google.com">
        <Icon name="Shoppers" />
        Shoppers
      </Menu.Item>
    </Sidebar>

    <Sidebar.Pusher>
      <Segment basic>
        <Grid.Column>
          <Menu size="small">
            <Menu.Item>Shopper Name</Menu.Item>
            <Menu.Item>
              <Dropdown text="Tipo">
                <Dropdown.Menu>
                  <Dropdown.Item text="New" />
                  <Dropdown.Item text="Normal" />
                  <Dropdown.Item text="Loco" />
                  <Dropdown.Item text="Loquisimo" />
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Item>
            <Menu.Item>
              <Dropdown text="Provincia">
                <Dropdown.Menu>
                  <Dropdown.Item text="Samana" />
                  <Dropdown.Item text="Santiago" />
                  <Dropdown.Item text="DN" />
                  <Dropdown.Item text="Moca" />
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Item>
            <Menu.Item>
              <Dropdown text="Estado">
                <Dropdown.Menu>
                  <Dropdown.Item text="New" />
                  <Dropdown.Item text="NO SE" />
                  <Dropdown.Item text="LO SE MUCHO MENOS" />
                  <Dropdown.Item text="TE PUEDES MORIR MALDITO" />
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Item>
            <Menu.Item>
              <Search size="small" />
            </Menu.Item>
          </Menu>
        </Grid.Column>
        <TableData />
      </Segment>
    </Sidebar.Pusher>
  </Sidebar.Pushable>
);

export default SidebarVisible;
