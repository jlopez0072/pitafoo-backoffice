import React from "react";
import { Table, Button, Checkbox } from "semantic-ui-react";

const TableData = () => (
  <Table celled size="small">
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Nombre</Table.HeaderCell>
        <Table.HeaderCell>Telefono</Table.HeaderCell>
        <Table.HeaderCell>Email</Table.HeaderCell>
        <Table.HeaderCell>Estado</Table.HeaderCell>
        <Table.HeaderCell>Accion</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Shopper</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Shopper</Button>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>Cell</Table.Cell>
        <Table.Cell>
          <Checkbox toggle />
        </Table.Cell>
        <Button href="http://www.google.com">Ver Shopper</Button>
      </Table.Row>
    </Table.Body>

    <Table.Footer>
      <Table.Row />
    </Table.Footer>
  </Table>
);

export default TableData;
